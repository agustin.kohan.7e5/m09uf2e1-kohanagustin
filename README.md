# M09UF2E1-KohanAgustin

## Exercici 1: esbrinar quantes línies té un fitxer de text (grans i petits)
 - Escriu una funció que obri un fitxer de text i compti quantes línies té.
- Crea un Thread que compti i mostri quantes línies té un fitxer (reutilitza el codi anterior). El nom del fitxer serà un atribut del Thread de tipus String.
 - Crea una classe de Prova que crida a 3 threads per esbrinar quantes línies té cada fitxer. Proveu amb fitxers grans i petits.
## Exercici 2: recull d'informació. Redacta un document tot responent, una a una, les següents preguntes:

 - Revisa la documentació de Microsoft c# Thread. Quins mètodes de la classe Thread troves que pots necessitar si vols fer concurrència, que fan i en quin cas ho faries servir?
 - Esbrina que és la instrucció Yield i per a que pot ser útil en concurrència.
 - Defineix les classes 
 - Interface IEnumerator
 - Interface IEnumerable
