﻿using System;
using System.Threading;

namespace m09uf2e1_kohanagustin
{
    class Class1
    {
        
        public static bool runBool;
        static void tal(string[] args)
        {


            OneOnlyThread();

            //TwoThreads();


        }


        static void OneOnlyThread()
        {
            Thread myFirstThread = new Thread(MyThreadFunc);

            Console.WriteLine("La implementació normal és un sol fil");

            myFirstThread.Start();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Aquesta linia està en el progrés principal");

            
            Thread mySecondThreadWithParam = new Thread(funcThreadParam);
            mySecondThreadWithParam.Start("Abcdefghi");
        }

        static void MyThreadFunc()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("La implementació normal és un sol fil");
        }

        static void funcThreadParam(object o)
        {
            string s = Convert.ToString(o);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("El codi és: " + s);
        }


        private static void TwoThreads()
        {
            int m = 0;
            int x = 0;

            runBool = true;

            Thread[] aThreads = new Thread[8];
            for (int i = 0; i < aThreads.Length; i++)
            {
                aThreads[i] = new Thread(ThreadMessage);
                aThreads[i].Start(i);
            }

            while (runBool)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"This is the main thread! Iteration: {m}");
                m++;
            }
        }

        static void ThreadMessage(object o)
        {
            int n = 0;

            while (runBool)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"This is the Thread number: {o} iteration: {n}");
                n++;

            }

        }
        
    }
}
