﻿/*
 * Autor: Agustin Kohan
 * Ejercicio: m09uf2e1
 */

using System;
using System.IO;
using System.Linq;
using System.Threading;

namespace m09uf2e1_kohanagustin
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine(Directory.GetCurrentDirectory());
            Directory.SetCurrentDirectory("../../../MyDocs");

            //Exercice1();
            //Exercice2();
            Exercice3();
            
        }

        private static void Exercice1() => LinesInFile("Don Qijote Cap 1.txt");

        private static void Exercice2()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Thread Thread1 = new Thread(LinesInFile);
            Thread1.Start("Don Qijote Cap 1.txt");
        }

        private static void Exercice3()
        {
            Thread Thread1 = new Thread(LinesInFile);
            Thread1.Start("Don Qijote Cap 1.txt");
            Thread Thread2 = new Thread(LinesInFile);
            Thread2.Start("Lazarillo de Tormes Cap 1.txt");
            Thread Thread3 = new Thread(LinesInFile);
            Thread3.Start("Toston.txt");
            Console.ForegroundColor = ConsoleColor.Yellow;
        }

        private static void LinesInFile(object path)
        {
            if (File.Exists(path.ToString()))
                Console.WriteLine("El archivo contiene {0} líneas", File.ReadLines(path.ToString()).Count());
            else
                Console.WriteLine("MAAAAAAAL");
        }
    }
}
